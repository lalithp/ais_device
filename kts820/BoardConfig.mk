# SPDX-License-Identifier: Apache-2.0
#
# Copyright (C) 2019 The Android Open-Source Project

include device/ais/common/boardconfig-common.mk

# Architecture
TARGET_ARCH := arm
TARGET_ARCH_VARIANT := armv7-a-neon
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi

TARGET_NO_BOOTLOADER := true
KERNEL_IMX_PATH := vendor/ais/linux-imx
KERNEL_DEFCONFIG := device/ais/kts820/kernel.config
KERNEL_DTB_FILE := device/ais/kts820/imx6q-kts820.dtb

TARGET_BOARD_INFO_FILE := device/ais/kts820/board-info.txt
