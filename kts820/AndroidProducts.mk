# SPDX-License-Identifier: Apache-2.0
#
# Copyright (C) 2019 The Android Open-Source Project
# Copyright (C) 2020 Roman Stratiienko (r.stratiienko@gmail.com)

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/kts820.mk

COMMON_LUNCH_CHOICES := \
    kts820-eng \
    kts820-user \
    kts820-userdebug
