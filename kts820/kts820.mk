# SPDX-License-Identifier: Apache-2.0
#
# Copyright (C) 2020 Roman Stratiienko (r.stratiienko@gmail.com)

$(call inherit-product, device/ais/kts820/device.mk)

PRODUCT_BOARD_PLATFORM := freescale
PRODUCT_NAME := kts820
PRODUCT_DEVICE := kts820
PRODUCT_BRAND := ais
PRODUCT_MODEL := KTS820
PRODUCT_MANUFACTURER := ais
PRODUCT_HAS_EMMC := true

