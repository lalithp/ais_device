#
# Copyright (C) 2019 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include build/make/target/board/BoardConfigMainlineCommon.mk

AB_OTA_UPDATER := false

TARGET_NO_KERNEL := true 

BOARD_USES_RECOVERY_AS_BOOT := true

# generic wifi
WPA_SUPPLICANT_VERSION := VER_0_8_X
BOARD_WPA_SUPPLICANT_DRIVER := NL80211
BOARD_HOSTAPD_DRIVER := NL80211

# --- Android images ---
BOARD_FLASH_BLOCK_SIZE := 512

# User image
TARGET_COPY_OUT_DATA := data
TARGET_USERIMAGES_USE_EXT4 := true
TARGET_USERIMAGES_SPARSE_EXT_DISABLED := true
BOARD_USERDATAIMAGE_FILE_SYSTEM_TYPE := ext4
#BOARD_USERDATAIMAGE_PARTITION_SIZE := 33554432

# System image
#TARGET_COPY_OUT_SYSTEM := system
ANDROID_COMPILE_WITH_JACK ?= false

# Cache image
BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_CACHEIMAGE_PARTITION_SIZE := 69206016 # 66MB

# Vendor image
BOARD_USES_VENDORIMAGE := true
BOARD_VENDORIMAGE_FILE_SYSTEM_TYPE := ext4
#BOARD_VENDORIMAGE_PARTITION_SIZE := 2147483648 # 2GB

# Boot image
BOARD_BOOTIMAGE_PARTITION_SIZE := 25165824
#BOARD_DTBIMAGE_PARTITION_SIZE := 1048576
BOARD_DTBOIMG_PARTITION_SIZE := 524288

# Root image
TARGET_COPY_OUT_ROOT := root
BOARD_BUILD_SYSTEM_ROOT_IMAGE := true

# Recovery image
TARGET_COPY_OUT_RECOVERY := recovery

BOARD_EXT4_SHARE_DUP_BLOCKS := true

# Dynamic partition
BOARD_BUILD_SUPER_IMAGE_BY_DEFAULT := false 

TARGET_RECOVERY_PIXEL_FORMAT := RGBX_8888
TARGET_RECOVERY_FSTAB := device/ais/common/fstab


BOARD_USES_TINYHAL_AUDIO := true
TINYCOMPRESS_TSTAMP_IS_LONG := true
TINYALSA_NO_ADD_NEW_CTRLS := true
TINYALSA_NO_CTL_GET_ID := true
#USE_CAMERA_STUB := true
BUILD_EMULATOR_OPENGL := true
USE_OPENGL_RENDERER := true
BOARD_USE_LEGACY_UI := true

BOARD_GPU_DRIVERS := lima kmsro
BOARD_USES_METADATA_PARTITION := true

BOARD_USES_PRODUCTIMAGE := true
BOARD_PRODUCTIMAGE_FILE_SYSTEM_TYPE := ext4

# Enable dex-preoptimization to speed up first boot sequence
DEX_PREOPT_DEFAULT := nostripping
WITH_DEXPREOPT := true
ART_USE_HSPACE_COMPACT := true

DEVICE_MANIFEST_FILE := device/ais/common/manifest.xml
DEVICE_MATRIX_FILE := device/ais/common/compatibility_matrix.xml

# SELinux support
BOARD_PLAT_PUBLIC_SEPOLICY_DIR   += device/ais/common/sepolicy/public
BOARD_PLAT_PRIVATE_SEPOLICY_DIR  += device/ais/common/sepolicy/private
BOARD_VENDOR_SEPOLICY_DIRS       += device/ais/common/sepolicy/vendor
